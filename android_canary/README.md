# android-studio-project-template


```
Android Studio Koala Feature Drop | 2024.1.2 Canary 4
Build #AI-241.15989.150.2412.11905440, built on May 30, 2024
Runtime version: 17.0.11+0-17.0.11b1207.24-11852314 x86_64
VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
macOS 14.5
GC: G1 Young Generation, G1 Old Generation
Memory: 2048M
Cores: 12
Metal Rendering is ON
Registry:
  ide.experimental.ui=true
```


### License 
```
 Apache License, Version 2.0
 
 Copyright 2024 mylhyz
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```