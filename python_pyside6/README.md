# PySide6-template

PySide6 项目模板

### 技术栈

- Python3
- PySide6 https://doc.qt.io/qtforpython-6/quickstart.html
- venv https://docs.python.org/3/library/venv.html


### 模板初始化

1. venv环境

```bash
python3 -m venv venv
```

2. 激活环境

```bash
source venv/bin/activate # linux
```


3. 安装依赖

```bash
pip install pyside6
```

4. 锁定依赖

```bash
pip freeze > requirements.txt
# 之后就可以通过以下方式安装依赖了
pip install -r requirements.txt
```

### License 
```
 Apache License, Version 2.0
 
 Copyright 2024 mylhyz
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```