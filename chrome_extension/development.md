# chrome 插件开发备忘

- content_scripts 只会在页面打开时植入并执行一次，后续如果不是浏览器刷新不会再次执行相关脚本
- background.service_worker 脚本会在后台一直执行，能够访问 chrome 相关 API，监听插件安装，插件被点击等 action
- popup 相关是一个独立的插件浮层，支持访问 chrome 相关 API
