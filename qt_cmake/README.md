# qt-cmake-template

## 生成 makefile

### MacOSX

```bash
cmake -B build -DCMAKE_PREFIX_PATH:STRING=~/Library/Qt5.12.12/5.12.12/clang_64
```

### Windows

#### x86 工具链

```cmd
cmake -B build -DCMAKE_PREFIX_PATH:STRING=C:\Qt\Qt5.12.12\5.12.12\msvc2017
```

#### x64 工具链

```cmd
cmake -B build -A x64 -T host=x64 -DCMAKE_PREFIX_PATH:STRING=C:\Qt\Qt5.12.12\5.12.12\msvc2017_64
```

## 生成 MacOSX 的图标 icns 文件

https://zhuanlan.zhihu.com/p/348599140
