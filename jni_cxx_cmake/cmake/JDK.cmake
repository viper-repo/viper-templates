# 处理JDK环境配置

if(NOT DEFINED ENV{JAVA_HOME})
    message(FATAL_ERROR "not defined environment variable : JAVA_HOME")
endif()

message(STATUS "configure JDK env...")

set(JDK_INCLUDE_DIR $ENV{JAVA_HOME}/include)
message(STATUS "JDK_INCLUDE_DIR : ${JDK_INCLUDE_DIR}")

if(WIN32)
    set(JDK_INCLUDE_PLATFORM_DIR $ENV{JAVA_HOME}/include/win32)
elseif(APPLE)
    set(JDK_INCLUDE_PLATFORM_DIR $ENV{JAVA_HOME}/include/darwin)
else()
    message(FATAL_ERROR "this system is not supported JDK")
endif()
message(STATUS "JDK_INCLUDE_PLATFORM_DIR : ${JDK_INCLUDE_PLATFORM_DIR}")
