# 构建 Android 上的可独立运行的程序

https://developer.android.com/ndk/guides/cmake#command-line_2


```bash
cmake \
    -DCMAKE_TOOLCHAIN_FILE=$NDK/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=$ABI \
    -DANDROID_PLATFORM=android-$MINSDKVERSION \
    $OTHER_ARGS
```


产物运行方式

```bash
adb push build/main /data/local/tmp
adb shell 'chmod 777 /data/local/tmp/main'
adb shell '/data/local/tmp/main'
```