#!/bin/bash

NDK=/Users/mylhyz/Library/Android/ndk/android-ndk-r26c
ABI=arm64-v8a
MINSDKVERSION=21
OTHER_ARGS=

echo $NDK

cmake \
    -DCMAKE_TOOLCHAIN_FILE=$NDK/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=$ABI \
    -DANDROID_PLATFORM=android-$MINSDKVERSION \
    $OTHER_ARGS \
    -B build